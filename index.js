const nodemailer = require('nodemailer');

module.exports = class Reporter {
  constructor(mailSettings, logReport = true) {
    const { transport, from, to } = mailSettings;
    this.mailer = nodemailer.createTransport(transport);
    this.mail = { from, to };
    this.logReport = logReport;
  }

  format({ date = new Date(), message = '', details = {}, err }) {
    if (typeof details === 'object') {
      details = JSON.stringify(details, null, 2);
    } else {
      details = 'No details.';
    }
    const result = `
------------------
[${date.toLocaleString()}] - ${message}

----- Stacktrace -----
${err.stack}

----- Details -----
${details}

------------------
  `;
    return { text: result, html: result };
  }

  setFormatter(fn) {
    this.format = fn || this.format;
  }
  async onReport () {}
  async report({ subject, message, details, err }) {
    const { from, to } = this.mail;
    const date = new Date()
    const { html, text } = this.format({ date, message, details, err });
    if (this.logReport) {
      console.log(subject, text);
    }
    const mail = await this.mailer.sendMail({
      from,
      to,
      subject,
      text,
      html,
    });
    const result = { subject, date, message, details, err, text, html, mail }
    try {
      await this.onReport(result)
    } catch (err) {
      console.log(err)
    }
    return result
  }
};
