# mail-reporter

mail-reporter is a package to send error reports to email using nodemailer

## Install

`npm install mail-reporter --save`

## Usage

#### Define reporter

```javascript
const Reporter = require('mail-reporter');

const reporter = new Reporter(
  {
    // mail settings
    transport: {
      host: 'smtp.mail.com',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: 'username',
        pass: 'password',
      },
    },
    from: 'Reports <username@mail.com>',
    to: 'reports@mail.com',
  },
  // log report
  false
);
```

#### Using

```javascript
try {
  // ...code
  throw new Error('Too many parameters');
} catch (err) {
  reporter.report({
    subject: 'Application error',
    message: err.message,
    details: { some: 'Property', asd: { aaa: 'bbb' } },
    err,
  });
}
```

#### onReport

```javascript
reporter.onReport = async (data) => {
  console.log('reporter.report fired:', data);
  // { subject, date, message, details, err, text, html, mail }
};
```

#### Custom report format

```javascript
function customFormatter({ date, message, details, err }) {
  if (typeof details === 'object') {
    details = JSON.stringify(details, null, 2);
  } else {
    details = 'No details.';
  }
  const result = `
------------------
[${date.toLocaleString()}] - ${message}

----- Stacktrace -----
${err.stack}

----- Details -----
${details}

------------------
  `;
  // must return { text, html } object
  return { text: result, html: result };
}
reporter.setFormatter(customFormatter);
```

#### Full code example to test all features

```javascript
const Reporter = require('mail-reporter');

const reporter = new Reporter(
  {
    // mail settings
    transport: {
      host: 'smtp.mail.com',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: 'username',
        pass: 'password',
      },
    },
    from: 'Reports <username@mail.com>',
    to: 'reports@mail.com',
  },
  // log report
  false
);

function customFormatter({ date = new Date(), message, details, err }) {
  if (typeof details === 'object') {
    details = JSON.stringify(details, null, 2);
  } else {
    details = 'No details.';
  }
  const result = `
------------------
[${date.toLocaleString()}] - ${message}

----- Stacktrace -----
${err.stack}

----- Details -----
${details}

------------------
  `;
  // must return { text, html } object
  return { text: result, html: result };
}

reporter.setFormatter(customFormatter);

reporter.onReport = async (data) => {
  console.log('reporter.report fired:', data);
  // { subject, date, message, details, err, text, html, mail }
};

// ---------------

try {
  // ...code
  throw new Error('Too many parameters');
} catch (err) {
  reporter.report({
    subject: 'Application error',
    message: err.message,
    details: { some: 'Property', asd: { aaa: 'bbb' } },
    err,
  });
}
```
